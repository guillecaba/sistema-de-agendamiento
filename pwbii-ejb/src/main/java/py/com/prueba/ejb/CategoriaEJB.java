package py.com.prueba.ejb;




import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import javax.ejb.LocalBean;
import py.com.prueba.modelo.Categoria;


@Stateless
@LocalBean
public class CategoriaEJB {
    @PersistenceContext(unitName="pwbiiPU")
    private EntityManager em;

    protected EntityManager getEm() {
        return em;
    }

    public Categoria get(Integer id) {
        return em.find(Categoria.class, id);
    }

    public void persist(Categoria entity){
        getEm().persist(entity);
    }
    public Categoria merge(Categoria entity){
        return (Categoria) getEm().merge(entity);
    }
    public void delete(Integer id){
        Categoria entity = this.get(id);
        this.getEm().remove(entity);
    }
    public void delete(Categoria entity){
        this.delete(entity.getIdCategoria());
    }
    @SuppressWarnings("unchecked")
    public List<Categoria> lista() {
        Query q = getEm().createQuery(
                "SELECT p FROM Categoria p");
        return (List<Categoria>) q.getResultList();
    }
    public Long total() {
        Query q = getEm().createQuery(
                "Select Count(p) from Categoria p");
        return (Long) q.getSingleResult();
    }
}
