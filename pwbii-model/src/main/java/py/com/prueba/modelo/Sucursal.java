/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author efunes
 */
@Entity
@Table(name = "sucursal")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sucursal.findAll", query = "SELECT s FROM Sucursal s"),
    @NamedQuery(name = "Sucursal.findByIdSucursal", query = "SELECT s FROM Sucursal s WHERE s.idSucursal = :idSucursal"),
    @NamedQuery(name = "Sucursal.findByNombre", query = "SELECT s FROM Sucursal s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Sucursal.findByDescripcion", query = "SELECT s FROM Sucursal s WHERE s.descripcion = :descripcion"),
    @NamedQuery(name = "Sucursal.findByIdLocal", query = "SELECT s FROM Sucursal s WHERE s.idLocal = :idLocal"),
    @NamedQuery(name = "Sucursal.findByLunesHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.lunesHoraApertura = :lunesHoraApertura"),
    @NamedQuery(name = "Sucursal.findByLunesHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.lunesHoraCierre = :lunesHoraCierre"),
    @NamedQuery(name = "Sucursal.findByMartesHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.martesHoraApertura = :martesHoraApertura"),
    @NamedQuery(name = "Sucursal.findByMartesHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.martesHoraCierre = :martesHoraCierre"),
    @NamedQuery(name = "Sucursal.findByMiercolesHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.miercolesHoraApertura = :miercolesHoraApertura"),
    @NamedQuery(name = "Sucursal.findByMiercolesHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.miercolesHoraCierre = :miercolesHoraCierre"),
    @NamedQuery(name = "Sucursal.findByJuevesHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.juevesHoraApertura = :juevesHoraApertura"),
    @NamedQuery(name = "Sucursal.findByJuevesHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.juevesHoraCierre = :juevesHoraCierre"),
    @NamedQuery(name = "Sucursal.findByViernesHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.viernesHoraApertura = :viernesHoraApertura"),
    @NamedQuery(name = "Sucursal.findByViernesHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.viernesHoraCierre = :viernesHoraCierre"),
    @NamedQuery(name = "Sucursal.findBySabadoHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.sabadoHoraApertura = :sabadoHoraApertura"),
    @NamedQuery(name = "Sucursal.findBySabadoHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.sabadoHoraCierre = :sabadoHoraCierre"),
    @NamedQuery(name = "Sucursal.findByDomingoHoraApertura", query = "SELECT s FROM Sucursal s WHERE s.domingoHoraApertura = :domingoHoraApertura"),
    @NamedQuery(name = "Sucursal.findByDomingoHoraCierre", query = "SELECT s FROM Sucursal s WHERE s.domingoHoraCierre = :domingoHoraCierre")})
public class Sucursal implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_sucursal")
    private Integer idSucursal;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "id_local")
    private int idLocal;
    @Column(name = "lunes_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date lunesHoraApertura;
    @Column(name = "lunes_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date lunesHoraCierre;
    @Column(name = "martes_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date martesHoraApertura;
    @Column(name = "martes_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date martesHoraCierre;
    @Column(name = "miercoles_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date miercolesHoraApertura;
    @Column(name = "miercoles_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date miercolesHoraCierre;
    @Column(name = "jueves_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date juevesHoraApertura;
    @Column(name = "jueves_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date juevesHoraCierre;
    @Column(name = "viernes_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date viernesHoraApertura;
    @Column(name = "viernes_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date viernesHoraCierre;
    @Column(name = "sabado_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date sabadoHoraApertura;
    @Column(name = "sabado_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date sabadoHoraCierre;
    @Column(name = "domingo_hora_apertura")
    @Temporal(TemporalType.TIME)
    private Date domingoHoraApertura;
    @Column(name = "domingo_hora_cierre")
    @Temporal(TemporalType.TIME)
    private Date domingoHoraCierre;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursal", fetch = FetchType.LAZY)
    private Collection<SucursalServicio> sucursalServicioCollection;
    @JsonIgnore
    @JoinColumn(name = "id_ciudad", referencedColumnName = "id_ciudad")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Ciudad idCiudad;
    @JsonIgnore
    @JoinColumn(name = "id_sucursal", referencedColumnName = "id_local", insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Local local;
    @JsonIgnore
    @JoinColumn(name = "id_mapa", referencedColumnName = "id_mapa")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Mapa idMapa;
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursal", fetch = FetchType.LAZY)
    private Collection<HorarioExcepcion> horarioExcepcionCollection;

    public Sucursal() {
    }

    public Sucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }

    public Sucursal(Integer idSucursal, String nombre, int idLocal) {
        this.idSucursal = idSucursal;
        this.nombre = nombre;
        this.idLocal = idLocal;
    }

    public Integer getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Integer idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdLocal() {
        return idLocal;
    }

    public void setIdLocal(int idLocal) {
        this.idLocal = idLocal;
    }

    public Date getLunesHoraApertura() {
        return lunesHoraApertura;
    }

    public void setLunesHoraApertura(Date lunesHoraApertura) {
        this.lunesHoraApertura = lunesHoraApertura;
    }

    public Date getLunesHoraCierre() {
        return lunesHoraCierre;
    }

    public void setLunesHoraCierre(Date lunesHoraCierre) {
        this.lunesHoraCierre = lunesHoraCierre;
    }

    public Date getMartesHoraApertura() {
        return martesHoraApertura;
    }

    public void setMartesHoraApertura(Date martesHoraApertura) {
        this.martesHoraApertura = martesHoraApertura;
    }

    public Date getMartesHoraCierre() {
        return martesHoraCierre;
    }

    public void setMartesHoraCierre(Date martesHoraCierre) {
        this.martesHoraCierre = martesHoraCierre;
    }

    public Date getMiercolesHoraApertura() {
        return miercolesHoraApertura;
    }

    public void setMiercolesHoraApertura(Date miercolesHoraApertura) {
        this.miercolesHoraApertura = miercolesHoraApertura;
    }

    public Date getMiercolesHoraCierre() {
        return miercolesHoraCierre;
    }

    public void setMiercolesHoraCierre(Date miercolesHoraCierre) {
        this.miercolesHoraCierre = miercolesHoraCierre;
    }

    public Date getJuevesHoraApertura() {
        return juevesHoraApertura;
    }

    public void setJuevesHoraApertura(Date juevesHoraApertura) {
        this.juevesHoraApertura = juevesHoraApertura;
    }

    public Date getJuevesHoraCierre() {
        return juevesHoraCierre;
    }

    public void setJuevesHoraCierre(Date juevesHoraCierre) {
        this.juevesHoraCierre = juevesHoraCierre;
    }

    public Date getViernesHoraApertura() {
        return viernesHoraApertura;
    }

    public void setViernesHoraApertura(Date viernesHoraApertura) {
        this.viernesHoraApertura = viernesHoraApertura;
    }

    public Date getViernesHoraCierre() {
        return viernesHoraCierre;
    }

    public void setViernesHoraCierre(Date viernesHoraCierre) {
        this.viernesHoraCierre = viernesHoraCierre;
    }

    public Date getSabadoHoraApertura() {
        return sabadoHoraApertura;
    }

    public void setSabadoHoraApertura(Date sabadoHoraApertura) {
        this.sabadoHoraApertura = sabadoHoraApertura;
    }

    public Date getSabadoHoraCierre() {
        return sabadoHoraCierre;
    }

    public void setSabadoHoraCierre(Date sabadoHoraCierre) {
        this.sabadoHoraCierre = sabadoHoraCierre;
    }

    public Date getDomingoHoraApertura() {
        return domingoHoraApertura;
    }

    public void setDomingoHoraApertura(Date domingoHoraApertura) {
        this.domingoHoraApertura = domingoHoraApertura;
    }

    public Date getDomingoHoraCierre() {
        return domingoHoraCierre;
    }

    public void setDomingoHoraCierre(Date domingoHoraCierre) {
        this.domingoHoraCierre = domingoHoraCierre;
    }

    @XmlTransient
    public Collection<SucursalServicio> getSucursalServicioCollection() {
        return sucursalServicioCollection;
    }

    public void setSucursalServicioCollection(Collection<SucursalServicio> sucursalServicioCollection) {
        this.sucursalServicioCollection = sucursalServicioCollection;
    }

    public Ciudad getIdCiudad() {
        return idCiudad;
    }

    public void setIdCiudad(Ciudad idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public Mapa getIdMapa() {
        return idMapa;
    }

    public void setIdMapa(Mapa idMapa) {
        this.idMapa = idMapa;
    }

    @XmlTransient
    public Collection<HorarioExcepcion> getHorarioExcepcionCollection() {
        return horarioExcepcionCollection;
    }

    public void setHorarioExcepcionCollection(Collection<HorarioExcepcion> horarioExcepcionCollection) {
        this.horarioExcepcionCollection = horarioExcepcionCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSucursal != null ? idSucursal.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sucursal)) {
            return false;
        }
        Sucursal other = (Sucursal) object;
        if ((this.idSucursal == null && other.idSucursal != null) || (this.idSucursal != null && !this.idSucursal.equals(other.idSucursal))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.Sucursal[ idSucursal=" + idSucursal + " ]";
    }
    
}
