/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.com.prueba.modelo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author guille
 */
@Entity
@Table(name = "sucursal_servicio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SucursalServicio.findAll", query = "SELECT s FROM SucursalServicio s"),
    @NamedQuery(name = "SucursalServicio.findByIdSucursalServicio", query = "SELECT s FROM SucursalServicio s WHERE s.idSucursalServicio = :idSucursalServicio"),
    @NamedQuery(name = "SucursalServicio.findByDuracion", query = "SELECT s FROM SucursalServicio s WHERE s.duracion = :duracion"),
    @NamedQuery(name = "SucursalServicio.findByPrecio", query = "SELECT s FROM SucursalServicio s WHERE s.precio = :precio"),
    @NamedQuery(name = "SucursalServicio.findByCapacidad", query = "SELECT s FROM SucursalServicio s WHERE s.capacidad = :capacidad")})
public class SucursalServicio implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_sucursal_servicio")
    private Integer idSucursalServicio;
    @Column(name = "duracion")
    private Integer duracion;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @Column(name = "precio")
    private BigDecimal precio;
    @Basic(optional = false)
    @Column(name = "capacidad")
    private int capacidad;
    @JoinColumn(name = "id_servicio", referencedColumnName = "id_servicio")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Servicio idServicio;
    @JoinColumn(name = "id_sucursal", referencedColumnName = "id_sucursal")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Sucursal idSucursal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursalServicio", fetch = FetchType.LAZY)
    private Collection<PersonaSucursalServicio> personaSucursalServicioCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSucursalServicio", fetch = FetchType.LAZY)
    private Collection<Reserva> reservaCollection;

    public SucursalServicio() {
    }

    public SucursalServicio(Integer idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public SucursalServicio(Integer idSucursalServicio, BigDecimal precio, int capacidad) {
        this.idSucursalServicio = idSucursalServicio;
        this.precio = precio;
        this.capacidad = capacidad;
    }

    public Integer getIdSucursalServicio() {
        return idSucursalServicio;
    }

    public void setIdSucursalServicio(Integer idSucursalServicio) {
        this.idSucursalServicio = idSucursalServicio;
    }

    public Integer getDuracion() {
        return duracion;
    }

    public void setDuracion(Integer duracion) {
        this.duracion = duracion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public Servicio getIdServicio() {
        return idServicio;
    }

    public void setIdServicio(Servicio idServicio) {
        this.idServicio = idServicio;
    }

    public Sucursal getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(Sucursal idSucursal) {
        this.idSucursal = idSucursal;
    }

    @XmlTransient
    public Collection<PersonaSucursalServicio> getPersonaSucursalServicioCollection() {
        return personaSucursalServicioCollection;
    }

    public void setPersonaSucursalServicioCollection(Collection<PersonaSucursalServicio> personaSucursalServicioCollection) {
        this.personaSucursalServicioCollection = personaSucursalServicioCollection;
    }

    @XmlTransient
    public Collection<Reserva> getReservaCollection() {
        return reservaCollection;
    }

    public void setReservaCollection(Collection<Reserva> reservaCollection) {
        this.reservaCollection = reservaCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSucursalServicio != null ? idSucursalServicio.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SucursalServicio)) {
            return false;
        }
        SucursalServicio other = (SucursalServicio) object;
        if ((this.idSucursalServicio == null && other.idSucursalServicio != null) || (this.idSucursalServicio != null && !this.idSucursalServicio.equals(other.idSucursalServicio))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.com.prueba.modelo.SucursalServicio[ idSucursalServicio=" + idSucursalServicio + " ]";
    }
    
}
